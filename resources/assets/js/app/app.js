(function($angular) {
    "use strict";
    var app = $angular.module('Kiberzauras', [
        'ngRoute',
        'ngResource',
        'ngAnimate',
        'ui.slimscroll',
        'ui.bootstrap']);

    app.constant('_', window._);

    app.config(function($routeProvider, $locationProvider) {
        $routeProvider.when('/', {
            template: 'hah'
        })
            .when('/reports', {
                templateUrl: laroute.route('admin::get::reports')
            })
            .when('/dashboard', {
                templateUrl: laroute.route('admin::get::dashboard')
            })
            .when('/else', {
                templateUrl: laroute.route('admin::get::else')
            });

        $locationProvider.html5Mode(true);
    });

    app.controller('MyCtl', function($scope) {
        $scope.somedata = 'testing 1 2 3';
    });

    app.directive('a', ['$document', '$location', '$route', function($document, $location, $route) {
        return function(scope, element, attrs) {
            element.bind('click',function() {
                if (element[0] && element[0].href && element[0].href === $location.absUrl()){
                    $route.reload();
                }
            });
        }
    }]);

    app.directive('applicationStarter',
        function( $animate ) {

            // Return the directive configuration.
            return({
                link: link,
                restrict: "C"
            });


            // I bind the JavaScript events to the scope.
            function link( scope, element, attributes ) {

                // Due to the way AngularJS prevents animation during the bootstrap
                // of the application, we can't animate the top-level container; but,
                // since we added "ngAnimateChildren", we can animated the inner
                // container during this phase.

                $animate.leave( element );
            //.then(
                    /*function cleanupAfterAnimation() {

                        // Remove the root directive element.
                        element.remove();

                        // Clear the closed-over variable references.
                        scope = element = attributes = null;

                    }*/
               // );

            }

        }
    );

}(window.angular));