<?php
/**
 * @author Rytis Grincevicius <rytis@kiberzauras.com>
 * @license MIT
 */
namespace Kiberzauras\Admin;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupRoutes($this->app->router);
        $this->registerHelpers();

        $this->loadViewsFrom(__DIR__.'/resources/views', 'admin');

        $this->setupPublishes();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerKiberzauras();
    }

    /**
     * Publishes vendor files to project
     * php artisan vendor:publish
     */
    private function setupPublishes()
    {
        //php artisan vendor:publish --tag="admin.config"
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('admin.php'),
        ], 'admin.config');

        //php artisan vendor:publish --tag="admin.views"
        $this->publishes([
            __DIR__.'/resources/views' => base_path('resources/views/vendor/admin'),
        ], 'admin.views');

        //php artisan vendor:publish --tag="admin.assets"
        $this->publishes([
            __DIR__.'/resources/assets' => public_path('assets/vendor/admin'),
        ], 'admin.assets');

        //php artisan vendor:publish --tag="admin.migrations"
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('migrations')
        ], 'admin.migrations');
    }

    /**
     * Setups routes for admin panel
     * @param Router $router
     */
    private function setupRoutes(Router $router)
    {
        if (! $this->app->routesAreCached()) {
//            $router->group(['namespace' => 'Kiberzauras\Admin\Http\Controllers'], function($router)
//            {
                require __DIR__.'/Http/routes.php';
//            });
        }
    }
    private function registerKiberzauras()
    {
        $this->app->bind('kiberzauras', function($app){
            return new Kiberzauras($app);
        });
    }

    /**
     * Includes helper functions to laravel
     */
    private function registerHelpers()
    {
        require_once __DIR__ . '/helpers.php';
    }
}