<?php

if (! function_exists('__')) {
    /**
     * Returns translated label
     *
     * @param string $label
     * @param string $language
     * @return string
     */
    function __($label = null, $language = null)
    {
        return $label;
    }
}
