<?php

Route::group(['prefix'=>'admin', 'as'=>'admin::'], function() {
    Route::get('/', ['as'=>'root', function() {
        return view('admin::application');
    }]);
    //ng-view routes
    Route::group(['prefix'=>'get', 'as'=>'get::'], function() {

        Route::get('/dashboard', ['as'=>'dashboard', function() {
            return view('admin::get.dashboard');
        }]);
        Route::get('/reports', ['as'=>'reports', function() {
            return 'Reports here';
        }]);
        Route::get('/else', ['as'=>'else', function() {
            return 'Else?';
        }]);
    });


    //templates
    Route::group(['prefix'=>'template', 'as'=>'template::'], function() {
        Route::get('/main', ['as'=>'main', function() {
            return view('admin::layouts.master');
        }]);
    });

    //catches all routes and returns startup application
    Route::get('/{all}', function() {
        return view('admin::application');
    })->where('all', '.*');
});