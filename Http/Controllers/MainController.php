<?php

/**
 * Created by PhpStorm.
 * User: Rytis
 * Date: 2015-10-04
 * Time: 20:16
 */
namespace Kiberzauras\Admin\Http\Controllers;

use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin::application');
    }
}
